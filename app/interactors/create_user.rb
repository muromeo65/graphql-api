class CreateUser
  include Interactor

  def call
    raise 'Nome não pode ser nulo' if context.name.blank?
    raise 'Email não pode ser nulo' if context.email.blank?
    raise 'Senha não pode ser nula' if context.password.blank?
    raise 'Confirmação da senha não pode ser nula' if context.password_confirmation.blank?

    user = User.where(email: context.email).first
    raise 'Email já cadastrado' if user.present?

    user = User.new(
      name: context.name,
      email: context.email,
      password: context.password,
      password_confirmation: context.password_confirmation,
      admin: context.admin
    )

    user.save

    context.user = user
  rescue => e
    context.fail!(error: e.message)
  end
end
