class EditUser
  include Interactor

  def call
    raise 'User_id não pode estar em branco'        if context.user_id.blank?
    raise 'Novas informações não podem ser nulas'   if validation_error

    user = User.where(id: context.user_id).first
    raise 'Usuário não encontrado' if user.blank?

    user.name       = context.name      if context.name.present?
    user.email      = context.email     if context.email.present?
    user.password   = context.password  if context.password.present?

    user.save
    context.user = user
  rescue => e
    context.fail!(error: e.message, context: context)
  end

  private

  def validation_error
    context.email.blank?      &&
    context.password.blank?   &&
    context.name.blank?
  end
end
