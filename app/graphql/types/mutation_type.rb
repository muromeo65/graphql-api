module Types
  class MutationType < Types::BaseObject
    field :login,         mutation: Mutations::Authentication
    field :create_user,   mutation: Mutations::UserCreate
    field :edit_user,     mutation: Mutations::UserEdit
    field :delete_user,   mutation: Mutations::UserDelete
  end
end
