module Types
  class ErrorsType < Types::BaseObject
    field :details, String, null: false
    field :full_message, [String], null: false

    def details
      object.details.to_json
    end
  end
end
