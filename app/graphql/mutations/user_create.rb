module Mutations
  class UserCreate < Mutations::BaseMutation
    argument :name,                   String, required: true
    argument :email,                  String, required: true
    argument :password,               String, required: true
    argument :password_confirmation,  String, required: true
    argument :admin,                  String, required: false

    field :user,   Types::UserType, null: true
    field :error,  String,          null: true

    def resolve(attributes)
      result = CreateUser.call(attributes)

      if result.success?
        { user: result.user }
      else
        { error: result.error }
      end
    end
  end
end
