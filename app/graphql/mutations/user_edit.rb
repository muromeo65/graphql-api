module Mutations
  class UserEdit < Mutations::BaseMutation
    argument :name,      String, required: false
    argument :email,     String, required: false
    argument :password,  String, required: false
    argument :admin,     String, required: false

    field :user,  Types::UserType,  null: true
    field :error, String,           null: true

    def resolve(attributes={})
      authenticate!

      result = EditUser.call(attributes.merge(user_id: current_user.id))

      if result.success?
        { user: result.user }
      else
        { error: result.error }
      end
    end
  end
end
