module Mutations
  class Authentication < Mutations::BaseMutation
    argument :email, String, required: true
    argument :password, String, required: true

    field :token, String, null: true
    field :error, String, null: true

    def resolve(email:, password:)
      token = AuthenticateUser.call(email, password)

      if token.success?
        { token: token.result }
      else
        { error: token.errors }
      end
    end
  end
end
