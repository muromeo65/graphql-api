module Mutations
  class UserDelete < Mutations::BaseMutation
    argument :user_id, String, required: true

    field :user,  Types::UserType, null: true
    field :error, String, null: true

    def resolve(attributes = {})
      authenticate!
      result = DeleteUser.call(attributes)

      if result.success?
        { user: result.user }
      else
        { error: result.errors }
      end
    end
  end
end
