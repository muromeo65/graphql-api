class ApplicationController < ActionController::API
  def current_user
    AuthorizeApiRequest.call(request.headers).result
  end
end
