class AddAdminToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :admin, :boolean
    change_column_default :users, :admin, false
    add_index :users, :email, unique: true
  end
end
