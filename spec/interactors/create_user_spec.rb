require 'rails_helper'

RSpec.describe CreateUser, type: :interactor do
  describe '.call' do
    context 'Quando vou criar um usuário novo' do
      context 'e passo os parametros errados' do
        it 'deverá retornar erro' do
          result = described_class.call()

          expect(result).to be_a_failure
        end
      end

      context 'e passo os paramtros corretos' do
        let!(:user) { create(:user, email: 'email@email.com') }

        it 'deverá retornar erro se já existir email' do
          result = described_class.call(
            name: 'teste',
            email: 'email@email.com',
            password: '123',
            password_confirmation: '123'
          )

          expect(result).to be_a_failure
        end

        it 'deverá crirar com sucesso' do
          result = described_class.call(
            name: 'teste',
            email: 'murilo@email.com',
            password: '123',
            password_confirmation: '123'
          )

          created = User.where(email: 'murilo@email.com').first
          expect(result.user.id).to eq(created.id)
        end
      end
    end
  end
end
