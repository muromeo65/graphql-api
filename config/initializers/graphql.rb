if Rails.env.development?
  GraphiQL::Rails.config.headers['Authorization'] = -> (context) {
    "eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoyLCJleHAiOjE1ODk4Mzg1NzN9.zG4IS4fnPIYqJEDSMYfAwaYmUSeQU4ifbkXeqc5BdJc"
  }
end
